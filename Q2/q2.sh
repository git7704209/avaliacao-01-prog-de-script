#!/bin/bash

a="/tmp"
b="/etc"
c="/bin"

d=$(ls -l $a | grep "^⁻" | wc -l)
e=$(ls -l $a | grep "^d" | wc -l)

f=$(ls -l $b | grep "^-" | wc -l)
g=$(ls -l $b | grep "^d" | wc -l)

h=$(ls -l $c | grep "^-" | wc -l)
i=$(ls -l $c | grep "^d" | wc -l)

echo "Quantidade de arquivos em $a: $d"
echo "Quantidade de diretórios em $a: $e"

echo "Quantidade de arquivos em $b: $f"
echo "Quantidade de diretórios em $b: $g"

echo "Quantidade de arquivos em $c: $h"
echo "Quantidade de diretórios em $c: $i"
