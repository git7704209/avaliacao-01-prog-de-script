#!/bin/bash

read -p "Primeiro nome: " a
read -p "Segundo nome: " b
read -p "Terceiro nome: " c
read -p "Quarto nome: " d

mkdir $a
touch $a/README.md
echo "$a" >> $a/README.md
echo "$(date "+%d-%m-%Y")" >> $a/README.md

mkdir $b
touch $b/README.md
echo "$b" >> $b/README.md
echo "$(date "+%d-%m-%Y")" >> $b/README.md

mkdir $c
touch $c/README.md
echo "$c" >> $c/README.md
echo "$(date "+%d-%m-%Y")" >> $c/README.md

mkdir $d
touch $d/README.md
echo "$d" >> $d/README.md
echo "$(date "+%d-%m-%Y")" >> $d/README.md
