#!/bin/bash

echo "Existem 4 tipos de variáveis: "
echo -e "1) Variáveis definidas pelo usuário: essas variável são declaradas dentro do próprio script. Ex:\na=10\nb="'"texto"'
echo -e "2) Variáveis pela entrada padrão: essas variáveis são recebidas pelo usuário através do comando read. Ex:\nread x "'#lê o valor digitado e atribui à variável x'
echo -e "3) Variáveis do sistema: são variáveis que trazem informações sobre o sistema. Ex:\n"'${USER}'"\n"'${PATH}'
echo -e "4) Variáveis automáticas: são variáveis definidas pelo bash. A atribuição do valor dessas variáveis é feita quando o usuário passa parâmetros adicionais ao executar o script. Ou seja, não é necessário usar o comando read para ler os valores digitados pelo usuário na linha de comando. Ex:\n"'a=$1 #atribui à a o primeiro parâmetro digitado pelo usuário'
